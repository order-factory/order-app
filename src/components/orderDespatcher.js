import React, { Component } from 'react';
import { API_BASE_URL, API_CREATE_ORDER_URL } from '../common/constants';
import { orderToDespatchable } from '../common/utils';

class OrderDespatcher extends Component {

    despatchOrder = async order => {
        let response = { success: false };
        try {
            const rawResponse = await fetch(`${API_BASE_URL}${API_CREATE_ORDER_URL}`, {
                method: "post",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    ...orderToDespatchable(order)
                })
            });
            response = await rawResponse.json();
        } catch (exception) {
            console.log('error!');
        }

        if (response.success) {
            this.props.completeOrder(order, response.id);
        }
    }

    render() {
        console.log(this.props.orders);
        return (
            <section id="order-despatcher">
                Current orders:

                {this.props.orders.map(order => (
                    <div key={order.id}>
                        <p>Name: {order.shipping_address.first_name} | Shipping Type: {order.shipping_lines.reduce((all, i) => all += ' '+i.title, '')} | Price: {order.total_price}£</p>
                        <button onClick={() => this.despatchOrder(order)}>Despatch this order</button>
                    </div>
                ))}
            </section>
        );
    }
}

export default OrderDespatcher;