import React, { Component } from 'react';
import orderImport1 from '../mocks/orderImport.1';
import orderImport2 from '../mocks/orderImport.2';
import orderImportBoilerplate from '../mocks/orderImportBoilerplate';

const initialOrderState = {
    text: '',
    error: null,
};

class OrderImporter extends Component {
    state = {
        currentOrder: { ...initialOrderState },
        orderMocks: [orderImport1, orderImport2, orderImportBoilerplate]
    }

    onChangeOrder = event => {
        this.setState({
            currentOrder: {
                text: event.target.value,
                error: null
            }
        });
    }

    orderMocker = num => {
        this.setState({
            currentOrder: {
                text: JSON.stringify(this.state.orderMocks[num], null, 4),
                error: null,
            }
        });
    }

    setImportError = error => {
        const { text: orderAsText } = this.state.currentOrder;
        return this.setState({
            currentOrder: {
                text: orderAsText,
                error
            }
        });
    }

    importOrder = () => {
        let order;

        try {
            order = JSON.parse(this.state.currentOrder.text);
        } catch (error) {
            return this.setImportError(`Couldn't process order - Please check syntax`);
        }

        if (!Array.isArray(order)) {
            return this.setImportError(`Couldn't process order - Imported order should be inclosed in an Array`);
        }

        this.props.addOrders(order);

        this.setState({ currentOrder: { ...initialOrderState } });
    }

    render() {
        return (
            <section id="order-importer">
                <div>
                    <button onClick={() => this.orderMocker(0)}>Use Mocked Order N° 1</button>
                </div>
                <div>
                    <button onClick={() => this.orderMocker(1)}>Use Mocked Order N° 2</button>
                </div>
                <div>
                    <button onClick={() => this.orderMocker(2)}>Use Order Boilerplate</button>
                </div>
                <div> 
                    <textarea className="import-area" value={this.state.currentOrder.text} onChange={this.onChangeOrder}></textarea>
                </div>
                <div>
                    <button onClick={this.importOrder}>Import Order in Factory</button>
                    {this.state.currentOrder.error ? <p className="import-error">Error: {this.state.currentOrder.error}</p> : ''}
                </div>
            </section>
        );
    }
}

export default OrderImporter;