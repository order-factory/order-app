import React, { Component } from 'react';

class OrderCompleted extends Component {

    render() {
        return (
            <section id="order-completed">
                Completed:

                {this.props.completedOrders.map(order => (
                    <div key={order.id}>
                        <p>Name: {order.shipping_address.first_name} | Shipping Type: {order.shipping_lines.reduce((all, i) => all += ' '+i.title, '')} | Price: {order.total_price}£ | Despatch ID: {order.despatch_id}</p>
                    </div>
                ))}
            </section>
        );
    }
}

export default OrderCompleted;