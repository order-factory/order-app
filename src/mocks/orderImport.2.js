export default [
    {
        "id": 103,
        "email": "marco@london.co.uk",
        "total_price": "292.20",
        "total_weight_grams": 2200,
        "order_number": 103,
        "shipping_lines": [
        {
            "id": 1,
            "title": "Same Day",
            "price": "289.00",
        }
        ],
        "shipping_address": {
        "first_name": "Marco",
        "address1": "1 London Street",
        "city": "London",
        "postcode": "STHLND",
        },
    },
    {
        "id": 104,
        "email": "carlo@london.co.uk",
        "total_price": "65.00",
        "total_weight_grams": 1100,
        "order_number": 104,
        "shipping_lines": [
        {
            "id": 104,
            "title": "Current Week",
            "price": "60.00",
        }
        ],
        "shipping_address": {
        "first_name": "Carlo",
        "address1": "2 London Street",
        "city": "London",
        "postcode": "STHLND",
        },
    }
];