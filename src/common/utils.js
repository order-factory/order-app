export const gramsToPounds = grams => grams * 0.0022046;

export const orderToDespatchable = order => {
    return {
        client: order.shipping_address.first_name,
        price: order.total_price,
        customer: order.email,
        parcel: {
            weight: gramsToPounds(order.total_weight_grams),
            courier: order.shipping_lines[0] ? order.shipping_lines[0].title : 'Generic Shipping'
        },
        address: {
            postcode: order.shipping_address.postcode,
        }
    };
}
