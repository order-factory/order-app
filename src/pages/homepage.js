import React, { Component } from 'react';
import OrderImporter from '../components/orderImporter';
import OrderDespatcher from '../components/orderDespatcher';
import OrderCompleted from '../components/orderCompleted';

class HomePage extends Component {
    state = {
        orders: [],
        completedOrders: []
    }

    addOrders = newOrders => {
        const { orders } = this.state;
        this.setState({ orders: [...orders, ...newOrders] });
    }

    completeOrder = (orderToComplete, despatchId) => {
        const { orders, completedOrders } = this.state;
        const orderIndex = orders.indexOf(orderToComplete);

        if (orderIndex > -1) {
            orders.splice(orders.indexOf(orderToComplete), 1);
            completedOrders.push({
                ...orderToComplete,
                despatch_id: despatchId
            });

            this.setState({
                orders,
                completedOrders
            });
        }
    }

    render() {
        const { orders, completedOrders } = this.state;

        return (
            <main className="App">
                <OrderImporter addOrders={this.addOrders} />
                <OrderDespatcher completeOrder={this.completeOrder} orders={orders} />
                <OrderCompleted completedOrders={completedOrders} />
            </main>
        );
    }
} 

export default HomePage;
